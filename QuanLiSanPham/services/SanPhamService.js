function SanPhamService() {
    this.layDSSP = function () {
        var promise = axios({
            method: 'get',
            url: 'https://630eebe037925634188387dd.mockapi.io/phone'
        });

        return promise;
    }
    this.themSP = function (sp) {
        var promise = axios({
            method: 'post',
            url: 'https://630eebe037925634188387dd.mockapi.io/phone',
            data: sp
        });


        return promise;
    }
    this.xoaSanPham = function (id) {
        var promise = axios({
            method: 'delete',
            url: `https://630eebe037925634188387dd.mockapi.io/phone/${id}`
        });

        return promise;
    }
    this.xemSanPham = function (id) {
        var promise = axios({
            method: 'get',
            url: `https://630eebe037925634188387dd.mockapi.io/phone/${id}`
        });

        return promise;
    }
    this.capNhatSanPham = function (id, sp) {
        var promise = axios({
            method: 'put',
            url: `https://630eebe037925634188387dd.mockapi.io/phone/${id}`,
            data: sp
        });
        return promise;
    }


    this.timKiemSP = function (mangSP, chuoiTK) {
        var mangTK = [];
        mangTK = mangSP.filter(function (sp) {
            return sp.name.toLowerCase().indexOf(chuoiTK.toLowerCase()) >= 0;
        });
        return mangTK;
    }
}