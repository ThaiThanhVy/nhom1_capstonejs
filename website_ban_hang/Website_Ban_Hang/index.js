import {
  btnPurchase,
  getPhoneList,
  phoneList,
  renderShopCart,
  searchIndex,
  searchPhone,
  tinhTong,
} from "./controller/phone.controller.js";

const userShopCart_LOCALSTORAGE = "userShopCart_LOCALSTORAGE";

var shopCart = [];
var userShopCartJson = localStorage.getItem(userShopCart_LOCALSTORAGE);
if (userShopCartJson != null) {
  shopCart = JSON.parse(userShopCartJson);

  renderShopCart(shopCart);
  tinhTong(shopCart);
}

getPhoneList();
btnPurchase();

let addToCart = (phoneID) => {
  let index = searchIndex(shopCart, phoneID);
  let index2 = searchIndex(phoneList, phoneID);
  // console.log(index, index2);

  if (index == -1) {
    let cartItem = { ...phoneList[index2], soLuong: 1 };
    // console.log("cartItem: ", cartItem);
    shopCart.push(cartItem);
    // console.log("shopCart: ", shopCart);
    renderShopCart(shopCart);
  } else {
    shopCart[index].soLuong++;
    renderShopCart(shopCart);
  }
  tinhTong(shopCart);
  localStorage.setItem(userShopCart_LOCALSTORAGE, JSON.stringify(shopCart));
  btnPurchase();
};

let changeAmount = (phoneID, value) => {
  let index = searchIndex(shopCart, phoneID);

  if (value) {
    shopCart[index].soLuong++;
    renderShopCart(shopCart);
  } else {
    if (shopCart[index].soLuong > 1) {
      shopCart[index].soLuong--;
      renderShopCart(shopCart);
    }
  }
  tinhTong(shopCart);
  localStorage.setItem(userShopCart_LOCALSTORAGE, JSON.stringify(shopCart));
};

let removeFromCart = (phoneID) => {
  let index = searchIndex(shopCart, phoneID);

  shopCart.splice(index, 1);
  renderShopCart(shopCart);
  tinhTong(shopCart);
  localStorage.setItem(userShopCart_LOCALSTORAGE, JSON.stringify(shopCart));
  btnPurchase();
};

let clearCart = () => {
  shopCart = [];
  renderShopCart(shopCart);
  tinhTong(shopCart);
  localStorage.setItem(userShopCart_LOCALSTORAGE, JSON.stringify(shopCart));
  btnPurchase();
};

let purchase = () => {
  document.getElementById("totalPay").innerHTML =
    document.getElementById("totalCharge").innerHTML;
};

let filterPhone = () => {
  searchPhone(phoneList, "filterPhone");
};

window.getPhoneList = getPhoneList;
window.addToCart = addToCart;
window.changeAmount = changeAmount;
window.removeFromCart = removeFromCart;
window.clearCart = clearCart;
window.purchase = purchase;
window.filterPhone = filterPhone;
